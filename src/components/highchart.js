import React from 'react'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import './highchart.css'
 
const options = (states, positiveCases,activeCases,deaths) =>( {
  chart: {
    type: 'column',
    width: 1500,
        height: 700
},
 
title: {
  text: 'Covid Infection -State wise India'
},
subtitle: {
  text: 'Open Source Gov API'
},
  
xAxis: {
  categories: states,
  title: {
      text: 'Affected no. in thousands'
  }
},

yAxis: {
  min: 0,
  title: {
      text: 'Population (thousands)',
      align: 'high'
  },
  labels: {
      overflow: 'justify'
  }
},
tooltip: {
  valueSuffix: 'Thousands'
},
plotOptions: {
  column: {
      pointPadding: 0.1,
      borderWidth: 0
  }
},
legend: {
  layout: 'vertical',
  align: 'right',
  verticalAlign: 'top',
  
  floating: true,
  borderWidth: 1,
  backgroundColor:
      Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
  shadow: true
},
credits: {
  enabled: false
},
series: [{
  name: 'Confirmed cases',
  data: positiveCases
}, {
  name: 'Active cases',
  data: activeCases
}, {
  name: 'Deaths',
  data: deaths
}, ]

  
})
 
const HighchartsDisplay = (props) => {
    console.log("Props",props.info);
    const states= props.info.map((info)=>{
        return info.state;
    })
    const positiveCases= props.info.map((info)=>{
        return parseFloat(info.confirmed);
    })
    const activeCases= props.info.map((info)=>{
      return parseFloat(info.active);
  })
  const deaths= props.info.map((info)=>{
    return parseFloat(info.deaths);
})
    
    console.log("Props-statesdsdsa", positiveCases);
    return (
        <div>
  <HighchartsReact id="chart"
    highcharts={Highcharts}
    options={options(states,positiveCases,activeCases,deaths)}
    oneToOne={true}
  />
</div>

    )
}
export default HighchartsDisplay