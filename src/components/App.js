import React from 'react';
import HighchartsDisplay from './highchart';
import Header from './header';
import {Container,Row,Col,Button} from  'react-bootstrap';
import covid from '../apis/covid'
import './highchart.css'


class App extends React.Component{

    state = { data: [] };

  componentDidMount() {
   this.onSearchSubmit()
  }

    onSearchSubmit = async term => {
      const response = await covid.get('data.json');
  
    this.setState({ data: response.data.statewise });
      console.log("dsadsadsa",response.data.statewise);
    };


    render(){
        return (
            <Container fluid>
             <HighchartsDisplay info={this.state.data} id="chart"/>
          
            
            </Container>

          
        )
    }
}

export default App